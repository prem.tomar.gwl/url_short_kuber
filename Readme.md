
---


Hi,

I've picked and completed the T-3 task, kindly find the details below

T-3
===========

### Frontend:
* I've used Angular 9 and Material Design to design basic form
* Menu and Logo place are made to be clearly visible and can be converted into hamburger
* You must login using admin/password this is hardcoded into system as no signup functionality is implemented
* JWT authentication system is in place to ensure secure access to resources, same can be implemented using Passport or oauth
* No HTTP_INTERCEPTORS used as there is no routing or complex guarding needed
* Custom URL's can be provided
* URL expires in 24h
* Only 100 request can be made in 1 minute window
* Can be accessed on *http://localhost:4201* via kubernetes
* Can be accessed on *https://localhost:4200* on local machine (node js application must be running in background)

### Backend:
* Nodejs is used as backend support
* API for login, processing URL is developed
* schema validation implemented
* JWT authentication implemented
* API controller is implemented to keep controll over traffic and make channel for each and every requests, this project did not required too complex struction so modularisation is on a bit lighter side.
* DB layer is to handler all the DB ops
* Module specific db ops are also considered
* Chained API processes
* Redis is used for DB support
* *npm start* to run is individually

### Deployment

* Docker & Kubernetes
* Docker images and kubernetes deployment is written
* Run this command *chmod +x url_shortner.sh; ./url_shortner.sh* from project directory
* Individual docker files are placed inside project directories

### Testing

* Integration testing
* Unit testing
* Using Mocha, Chai in Backend
* *npm test* inside backend project directory *Test is automated in docker*

### Installation

installation of kubernetes, docker and other prerequisites is not implemented as those platform based

docker compose file is also present in project directory...

Note: Suitable for Linux and Mac 

Tested on Mac OS X: 10.15.3 (19D76)





