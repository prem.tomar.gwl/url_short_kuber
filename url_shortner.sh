#!/bin/bash

sudo docker build --no-cache -t url_api:1.0 url-shorten-api/.
sudo docker build --no-cache -t url_ui:1.0 url-shorten-ui/.
sudo kubectl apply -k ./
