
let jwt = require('jsonwebtoken')
let secret = require('../utils/config').secret
let q = require('q')
let responseBuilder = require('../utils/response').buildResponse

let login = (data) => {
  let deferred = q.defer()
  let username = data.username;
  let password = data.password;
  // For the given username fetch user from DB
  let mockedUsername = 'admin';
  let mockedPassword = 'password';

  if (username && password) {
    if (username === mockedUsername && password === mockedPassword) {
      let token = jwt.sign({ username: username },
        secret,
        {
          expiresIn: '24h' // expires in 24 hours
        }
      );
      // return the JWT token for the future API calls

      deferred.resolve(responseBuilder(202, { token: token }, null, "Authenticated"))
    } else {
      deferred.reject(responseBuilder(403, null, null, "Incorrect username or password"))

    }
  } else {
    deferred.reject(responseBuilder(400, null, null, "Authentication failed! Please check the request"))

  }

  return deferred.promise
}

module.exports = {
  login
}