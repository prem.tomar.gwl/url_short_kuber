var Schema = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "URL Validator",
  "description": "Validate incoming URL request",
  "type": "object",
  "properties": {
    "username": {
      "description": "Username",
      "type": "string"
    },
    "password": {
      "description": "Password",
      "type": "string"
    }
  },
  "required": ["username", "password"]
};

module.exports = Schema;
