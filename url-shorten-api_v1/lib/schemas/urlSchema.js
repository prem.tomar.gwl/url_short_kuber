var Schema = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "URL Validator",
  "description": "Validate incoming URL request",
  "type": "object",
  "properties": {
    "url": {
      "description": "Original URL to shoten",
      "type": "string"
    },
    "custom_url": {
      "description": "Custom url",
      "type": "string"
    }
  },
  "required": ["url"]
};

module.exports = Schema;
