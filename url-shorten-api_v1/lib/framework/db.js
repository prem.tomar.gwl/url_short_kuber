var q = require('q')
var redis = require('redis')

var client = redis.createClient(process.env.REDIS_PORT || "6379", process.env.REDIS_URL || "127.0.0.1");
// let client = null;

module.exports = {
    client
}