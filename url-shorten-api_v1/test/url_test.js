let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../testing');
let should = chai.should();
var expect = chai.expect
let token = "";
let shortUrl = "";

chai.use(chaiHttp);

describe('/api/v1/auth/login', () => {
    it('Should throw schema validation error for missing password', (done) => {
        var body = {
            username: "admin",
        }
        chai.request(server)
            .post('/api/v1/auth/login')
            .send(body)
            .end((err, res) => {
                res.should.have.status(400);
                console.log({ validation: res.body })
                done();
            });
    });
});


describe('/api/v1/auth/login', () => {
    it('Should throw schema validation error for integer type password value', (done) => {
        var body = {
            username: "admin",
            password: 123
        }
        chai.request(server)
            .post('/api/v1/auth/login')
            .send(body)
            .end((err, res) => {
                res.should.have.status(400);
                console.log({ validation: res.body })
                done();
            });
    });
});

describe('/api/v1/auth/login', () => {
    it('Should be able to login', (done) => {
        var body = {
            username: "admin",
            password: "password",
        }
        chai.request(server)
            .post('/api/v1/auth/login')
            .send(body)
            .end((err, res) => {
                res.should.have.status(202);
                expect(res.body).to.have.property('token')
                token = res.body.token;
                done();
            });
    });
});

describe('/api/v1/getShortUrl', () => {
    it('Should throw 401 error', (done) => {
        let body = {
            url: "https://google.com"
        }
        chai.request(server)
            .post('/api/v1/getShortUrl')
            .send(body)
            .end((err, res) => {
                res.should.have.status(401);
                console.log(res.body)
                expect(res.body).to.have.property('err')
                expect(res.body.err).to.have.property('message').equal("No token provided")
                done();
            })
    })
})



describe('/api/v1/getShortUrl', () => {
    it('Should be able to get short url without custom url', (done) => {
        let body = {
            url: "https://google.com"
        }
        chai.request(server)
            .post('/api/v1/getShortUrl')
            .set('authorization', token)
            .send(body)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property('url')
                console.log(res.body.url);
                shortUrl = res.body.url;
                done();
            })
    })
})

describe('/api/v1/getShortUrl', () => {
    it('Should be able to get short url with custom url', (done) => {
        let body = {
            url: "https://google.com",
            customUrl: "customUrl"
        }
        chai.request(server)
            .post('/api/v1/getShortUrl')
            .set('authorization', token)
            .send(body)
            .end((err, res) => {
                res.should.have.status(200);
                expect(res.body).to.have.property('url')
                console.log(res.body.url);
                shortUrl = res.body.url;
                done();
            })
    })
})

describe('/api/v1/:customUrl', () => {
    it('Should be able to access and send redirect instructions', (done) => {

        chai.request(server)
            .get(shortUrl)
            .end((err, res) => {
                res.should.have.status(200);
                res.redirects.should.be.an('array')
                done();
            })
    })
})

describe('/api/v1/:customUrl', () => {
    it('Should block more than 100 request within 1 min duration', (done) => {


        setInterval(() => {

            chai.request(server)
                .get(shortUrl)
                .end((err, res) => {
                    console.log(res.status)

                })



        }, 500)

        setTimeout(() => {
            done();
        }, 60000)
    })
})