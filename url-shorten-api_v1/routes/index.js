var express = require('express');
var router = express.Router();
var Controller = require('../lib/framework/controller')
var urlMod = require('../lib/url')
var auth = require('../lib/auth')
var rateLimiter = require('redis-rate-limiter');
var client = require('../lib/framework/db').client;

let urlService = new Controller({
    getShortUrl: urlMod.getShortUrl,
    redirectUrl: urlMod.redirectUrl,
    login: auth.login
}, {
    login: {
        auth: false,
        schema: "schemas/login"
    },
    getShortUrl: {
        auth: true,
        schema: "schemas/urlSchema"
    },
    redirectUrl: {
        auth: false
    }
})

let getKey = (req) => {
    return req.params.url;
}

router.post('/getShortUrl', urlService.getShortUrl);
router.get('/:url', rateLimiter.middleware({ redis: client, key: getKey, rate: '100/minute' }), urlService.redirectUrl)
router.post('/auth/login', urlService.login)
module.exports = router;