import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MainRequest, MainResponse, Auth } from '../interfaces/main';
import { Observable } from 'rxjs';
import { resolve } from 'dns';
import { rejects } from 'assert';
import { AuthUtils } from '../utils/auth';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  headers = {
    headers: {
      'Content-type': 'application/json'
    }
  }
  constructor(private http: HttpClient, private authUtil: AuthUtils) { }

  /**
   * @param body Pass request data object to send along with post request
   * @description To get short url of original url 
   */
  getShortUrl = (body: MainRequest): Promise<MainResponse> => {
    this.headers.headers['authorization'] = this.authUtil.Token;
    console.log(this.headers)
    return new Promise((resolve, reject) => {
      this.http.post<MainResponse>("http://34.93.40.42:3001" + environment.apis.getShortUrl, body, this.headers).subscribe(data => {
        resolve(data as MainResponse)
      }, error => {
        reject(error);
      })
    })
  }

  login = (auth: Auth): Promise<Auth> => {

    return new Promise((resolve, reject) => {
      this.http.post<Auth>("http://34.93.40.42:3001" + environment.apis.login, auth, this.headers).subscribe(data => {
        resolve(data as Auth)
      }, error => {
        reject(error);
      })
    })
  }
}
