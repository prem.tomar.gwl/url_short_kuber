export class MainRequest {
  url: string;
  customUrl?: string;
}

export interface MainResponse {
  url: string;
}

export class Auth {
  username: string;
  password: string;
  token: string;
}
