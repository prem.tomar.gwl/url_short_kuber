import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MainService } from './services/main.service';
import { MainRequest, MainResponse, Auth } from './interfaces/main';
import { error } from 'protractor';
import { Formatter } from './formatter'
import { AuthUtils } from './utils/auth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'URL Shortner';
  url: string = "https://google.co.in"
  urlToShort: MainRequest;
  auth: Auth
  form: FormGroup;
  loggedIn: boolean = false;
  constructor(private builder: FormBuilder,
    private service: MainService,
    private formatter: Formatter,
    private authUtil: AuthUtils) {
    this.urlToShort = new MainRequest()
    this.auth = new Auth();
    this.form = this.builder.group({
      original_url: new FormControl('', [Validators.required, Validators.pattern("(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?")]),
      custom_url: new FormControl('')
    })
  }

  makeItShort = () => {
    this.formatter.formatRequest(this.urlToShort)
      .then(this.service.getShortUrl)
      .then((data: MainResponse) => {
        this.url = data.url
      })
      .catch(error => {
        throw error;
      })
  }

  login = () => {
    this.service.login(this.auth)
      .then((data: Auth) => {
        console.log(data.token)
        this.loggedIn = true;
        this.authUtil.Token = data.token
      })
  }

}
